package luitzenjan;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import sun.rmi.runtime.Log;

import java.io.File;
import java.util.Scanner;

import static java.lang.Thread.sleep;




public abstract class Main extends playMusic {

    private static String input;
    private static MediaPlayer mediaPlayer;
    private static Scanner scanner = new Scanner(System.in);
    private static int counter = 0;
    private static Thread thread;


    public static void main(String[] args) throws Exception {

        String bip = "/Users/luitzen-janbeiboer/Downloads/OliverHeldens-WhatTheFunkft.DannyShah.mp3";
        Media hit = new Media(new File(bip).toURI().toString());
        mediaPlayer = new MediaPlayer(hit);

        System.out.println("Press the enter key to activate the program.");

        while (true) {
            input = scanner.nextLine();

            if (input.equals("")) {
                if (counter == 0) {
                    counter++;
                    thread = newThread();
                    thread.start();
                } else {
                    System.out.println("I'm already started.");
                }
            }
        }
    }

    static public Thread newThread() {
        return new Thread(() -> {
            try {
                sleep(500); //  0.5 sec
                System.out.println("I'm playing.");
                mediaPlayer.play();
                sleep(10000); // 10 sec.
                mediaPlayer.pause();
                System.out.println("I'm pause");
                counter = 0;

                mediaPlayer.setCycleCount(MediaPlayer.INDEFINITE);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

}
